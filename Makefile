NAME:=libcii
VER:=master
SRC:=master.zip
UNPACK:=unzip
SRCURL:=https://github.com/kev009/cii/archive/master.zip
BUILDDIR:=cii-$(VER)
MAKEOPT:=THREADS=
PATH:=$(PATH):$(STAGEDIR)/bin
CONFIGUREOPT:=--host=$(HOST) --prefix=$(PREFIX) CFLAGS="$(CFLAGS)" LDFLAGS="$(LDFLAGS)" LIBS="$(LIBS)"
TARGET:=src/.libs/$(NAME).a
PATCHES:=$(wildcard patches/*.patch)
INSTALL_TARGET:=install
PACKAGE:=$(NAME)-$(VER)$(PKGSUFFIX).tgz
PREFIX?=/usr/local
DESTDIR?=/
STAGEDIR?=$(PREFIX)/

all: build

download: .downloaded
.downloaded: $(SRC)
	@touch $@
$(SRC):
	wget $(SRCURL)

unpack: .unpacked
.unpacked: .downloaded
	$(UNPACK) $(SRC)
	@touch $@
	
patch: .patched
.patched: .unpacked $(PATCHES)
	[ -z "$(PATCHES)" ] || cat $(PATCHES) | patch -d $(BUILDDIR) -p1
	cp -f cii-makefile $(BUILDDIR)/makefile
	@touch $@

configure: .configured
.configured: .patched
	touch $@

build: .builded
.builded: $(BUILDDIR)/$(TARGET)
	$(MAKE) -C $(BUILDDIR) $(MAKEOPT) PREFIX=$(PREFIX)
	touch $@

$(BUILDDIR)/$(TARGET): .patched
	$(MAKE) -C $(BUILDDIR) $(MAKEOPT)
	
install: .builded
	mkdir -p $(DESTDIR) ;\
	cd $(DESTDIR); \
	d=`pwd`; \
	cd -; \
	$(MAKE) -C $(BUILDDIR) $(MAKEOPT) DESTDIR=$$d PREFIX=$(PREFIX) $(INSTALL_TARGET)

stage: install
	rsync -av $(DESTDIR)$(PREFIX)/ $(STAGEDIR)
	-sed -i -e "s#^prefix=.*#prefix=$(STAGEDIR)#" $(STAGEDIR)lib/pkgconfig/$(subst lib,,$(NAME)).pc
	
package: $(PACKAGE)
$(PACKAGE): install
	tar czf $@ -C $(DESTDIR)$(PREFIX) --owner root --group root .

clean:
	-rm -f .builded .configured
	-$(MAKE) -C $(BUILDDIR) dist$@
	-rm -rf $(DESTDIR)

distclean:
	-rm -rf $(BUILDDIR) $(DESTDIR)
	-$(MAKE) clean
	-rm -f .*ed
